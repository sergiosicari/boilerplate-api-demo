#!make

include .env
include .env.local

export

download_composer: ## Download composer
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php composer-setup.php
	php -r "unlink('composer-setup.php');"

install: ## Install composer
	php composer.phar install

drop_db:
	php bin/console doctrine:database:drop --env=${APP_ENV} --force

create_db: ## Create DB
	php bin/console doctrine:database:create --env=${APP_ENV}

build_db:
	php bin/console doctrine:migrations:migrate --no-interaction --env=${APP_ENV}

cs_fix: ## Apply code sniffer fix
	./vendor/bin/php-cs-fixer fix --verbose --config=./.php_cs.dist ./src/

install_all:
	make download_composer && make install && make drop_db && make create_db && make build_db

test:
	php bin/phpunit

docker_start: ## start your containers
	docker-compose up -d
