<?php

namespace App\Exception;

class UserNotEnabledException extends \Exception
{
    public function __construct($message = 'User not enabled')
    {
        parent::__construct($message);
    }
}
