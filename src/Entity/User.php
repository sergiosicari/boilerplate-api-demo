<?php

namespace App\Entity;

use App\DBAL\Types\RoleType;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @JMS\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string The hashed password
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Type("string")
     * @JMS\Exclude()
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * As we are adding some dynamic role, we will use this attribute to return array of user roles as strings
     * (not directly from the relation because some of them does not actually exist,
     * for instance the ROLE_CUSTOMER_OWNER).
     *
     * Furthermore getRoles() is used by the security component of symfony to check
     * roles, and it must be an array of (string) names.
     *
     * @var array
     *
     * @JMS\Groups({"admin", "profile"})
     * @JMS\Accessor("getRoles")
     */
    private $roles;

    /**
     * Just used to define and get data from the relation.
     * Use roles or getRoles to access user current roles as an array of string names.
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", inversedBy="users")
     * @ORM\OrderBy({"id" = "ASC"})
     *
     * @JMS\Exclude();
     */
    private $relationRoles;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get user role names as an array of string.
     */
    public function getRoles(): array
    {
        $roles = $this->relationRoles->map(function (Role $role) {
            return $role->getName();
        })->toArray();

        $roles[] = RoleType::ROLE_USER;

        return $roles;
    }

    /**
     * @return Collection|Role[]
     */
    public function getRelationRoles(): Collection
    {
        return $this->relationRoles;
    }

    /**
     * @return User
     */
    public function addRole(Role $role): self
    {
        if (!$this->relationRoles->contains($role)) {
            $this->relationRoles[] = $role;
            $role->addUser($this);
        }

        return $this;
    }

    /**
     * @return User
     */
    public function removeRole(Role $role): self
    {
        if ($this->relationRoles->contains($role)) {
            $this->relationRoles->removeElement($role);
            $role->removeUser($this);
        }

        return $this;
    }

    public function getPassword(): ?string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return (string) $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
