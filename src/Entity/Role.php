<?php


namespace App\Entity;

use App\DBAL\Types\RoleType;
use Assert\Assertion;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 * @ORM\Table("role")
 */
class Role
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="role_type", nullable=false)
     *
     * @JMS\Type("string")
     * @JMS\Groups({"list", "details"})
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\RoleType")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="relationRoles")
     * @ORM\OrderBy({"id" = "DESC"})
     *
     * @JMS\Exclude();
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Permission", mappedBy="roles")
     * @ORM\OrderBy({"id" = "DESC"})
     *
     * @JMS\Exclude();
     */
    private $permissions;

    /**
     * Role constructor.
     *
     * @throws \Exception
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(string $name)
    {
        $this->setName($name);
        $this->users = new ArrayCollection();
        $this->permissions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Role
     *
     * @throws \Assert\AssertionFailedException
     */
    public function setName(string $name): self
    {
        Assertion::inArray($name, RoleType::getChoices());

        $this->name = $name;

        return $this;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @return Role
     */
    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addRole($this);
        }

        return $this;
    }

    /**
     * @return Role
     */
    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeRole($this);
        }

        return $this;
    }

    public function getPermissions(): Collection
    {
        return $this->permissions;
    }

    /**
     * @return Role
     */
    public function addPermission(Permission $permission): self
    {
        if (!$this->permissions->contains($permission)) {
            $this->permissions[] = $permission;
            $permission->addRole($this);
        }

        return $this;
    }

    /**
     * @return Role
     */
    public function removePermission(Permission $permission): self
    {
        if ($this->permissions->contains($permission)) {
            $this->permissions->removeElement($permission);
            $permission->removeRole($this);
        }

        return $this;
    }

    public function hasPermission(Permission $permission): bool
    {
        return $this->permissions->contains($permission);
    }
}
