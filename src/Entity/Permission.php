<?php

namespace App\Entity;

use App\DBAL\Types\PermissionType;
use Assert\Assertion;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PermissionRepository")
 * @ORM\Table("permission")
 */
class Permission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="permission_type", nullable=false, unique=true)
     *
     * @JMS\Type("string")
     * @JMS\Groups({"list", "details"})
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\PermissionType")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", inversedBy="permissions")
     * @ORM\OrderBy({"id" = "DESC"})
     *
     * @JMS\Exclude();
     */
    private $roles;

    /**
     * Permission constructor.
     *
     * @throws \Exception
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(string $name)
    {
        $this->setName($name);
        $this->roles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Permission
     *
     * @throws \Assert\AssertionFailedException
     */
    public function setName(string $name): self
    {
        Assertion::inArray($name, PermissionType::getChoices());

        $this->name = $name;

        return $this;
    }

    public function getRoles(): Collection
    {
        return $this->roles;
    }

    /**
     * @return Permission
     */
    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
            $role->addPermission($this);
        }

        return $this;
    }

    /**
     * @return Permission
     */
    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
            $role->removePermission($this);
        }

        return $this;
    }
}
