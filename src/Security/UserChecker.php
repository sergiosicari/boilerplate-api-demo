<?php

namespace App\Security;

use App\Entity\User;
use App\Exception\UserNotEnabledException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker.
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param UserInterface $user
     * @throws UserNotEnabledException
     */
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

        if (!$user->isEnabled()) {
            throw new UserNotEnabledException();
        }
    }

    public function checkPreAuth(UserInterface $user)
    {
    }
}
