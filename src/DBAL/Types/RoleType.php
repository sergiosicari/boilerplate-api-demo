<?php

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class RoleType extends AbstractEnumType
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_USER = 'ROLE_USER'; // virtual. not added in $choices

    protected static $choices = [
        self::ROLE_ADMIN => self::ROLE_ADMIN,
    ];

    public static function getChoices(): array
    {
        return self::$choices;
    }
}
