<?php

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class PermissionType extends AbstractEnumType
{
    public const USER_VIEW = 'USER_VIEW';
    public const USER_EDIT = 'USER_EDIT';

    protected static $choices = [
        self::USER_VIEW => self::USER_VIEW,
        self::USER_EDIT => self::USER_EDIT,
    ];

    public static function getChoices(): array
    {
        return self::$choices;
    }
}
